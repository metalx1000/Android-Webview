# Android-Webview

Copyright Kris Occhipinti 2022-11-23

(https://filmsbykris.com)

License GPLv3

- This project is based on the example code at https://github.com/slymax/webview

# setup
Based on the following tutorial
https://proandroiddev.com/how-to-setup-android-sdk-without-android-studio-6d60d0f2812a

- Get Android SDK Command line tools https://developer.android.com/studio
```bash
wget "https://dl.google.com/android/repository/commandlinetools-linux-9123335_latest.zip" -O /tmp/commandlinetools.zip
mkdir ~/Android
cd ~/Android
unzip /tmp/commandlinetools.zip
cd cmdline-tools
mkdir tools
mv -i * tools

#run the following and add it to your shell's rc file
export ANDROID_HOME=$HOME/Android
export PATH=$ANDROID_HOME/cmdline-tools/tools/bin/:$PATH
export PATH=$ANDROID_HOME/emulator/:$PATH
export PATH=$ANDROID_HOME/platform-tools/:$PATH
```
- Install the sdk
```bash
sdkmanager --list
#change version if needed
yes|sdkmanager --install "platform-tools" "platforms;android-29" "build-tools;29.0.2"

#if you don't need the emulator you can save a lot of space by deleting it
rm -fr ../emulator
```

# Customize
- icon
```bash
#replace ./app/src/main/ic_launcher-web.png with your icon then copy to res
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/res/mipmap-xxhdpi/ic_launcher.png
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/res/mipmap-xxxhdpi/ic_launcher.png
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/res/mipmap-xhdpi/ic_launcher.png
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/res/mipmap-mdpi/ic_launcher.png
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/res/mipmap-hdpi/ic_launcher.png
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/assets/icon.png
```

- Rename App
```bash
echo -n "App ID (example: com.example.app): "
read appid
grep -ir com.example.app *|cut -d\: -f1|while read file;
do 
  sed -i "s/com.example.app/$appid/g" "$file"
done

echo -n "App Name: "
read app
sed -i "s/WebView App/$app/g" app/src/main/res/values/strings.xml
```

- Update Domain and URL
```bash
echo -n "URL: "
read url
#sed -i "s|https://example.com|$url|g" app/src/main/java/com/example/app/MainActivity.java
echo "window.location.replace('$url');" > "app/src/main/assets/index.js"

echo -n "Domain: "
read domain
sed -i "s|example.com|$domain|g" app/src/main/java/com/example/app/MyWebViewClient.java

```

- Build and Install It
```bash
./gradlew build && adb install app/build/outputs/apk/debug/app-debug.apk
```
