#!/bin/bash
######################################################################
#Copyright (C) 2022  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

git="https://gitlab.com/metalx1000/Android-Webview/-/archive/master/Android-Webview-master.zip"

[[ "$1" ]] && url="$1" || read -p "Enter Site to Package: " url
[[ "$url" ]] || exit 1
[[ "$url" == "http"* ]] || exit 1

domain="$(echo "$url" | sed -e 's|^[^/]*//||' -e 's|/.*$||')"

function error(){
  echo "$*"
  exit 1
}

function get_favicon_url(){
  html="$(wget -qO- --user-agent="apk" "$url")"
  list="$(echo "$html"|grep icon|grep link|sed 's/href="/\n/g'|cut -d\" -f1|grep '.png'|grep -v link)"
  let count="$(echo "$list"|wc -l)"
  [[ "$count" == "1" ]] && favicon="$list"
  [[ "$count" > "1" ]] && favicon="$(echo "$list" |fzf --prompt="Select Icon")"
  [[ "$favicon" ]] || read -p "Enter URL for Icon: " favicon
  [[ "$favicon" ]] || return
  [[ "$favicon" == "http"* ]] && return
  u="$(dirname "$url")"
  [[ "$favicon" == "/"* ]] && favicon="https://${domain}${favicon}" || favicon="${u}/${favicon}"

}

read -p "App Name: " app
[[ "$app" ]] || exit 1

read -p "App ID (example: com.example.app): " appid
[[ "$appid" ]] || appid="com.$USER.app"
echo "Using $appid"

wget "$git" -O /tmp/Android-Webview-master.zip
unzip "/tmp/Android-Webview-master.zip"
mv Android-Webview-master "$appid"

cd "$appid"

get_favicon_url
echo "Favicon URL is $favicon"
[[ "$favicon" ]] && wget --user-agent="apk" "$favicon" -O "app/src/main/ic_launcher-web.png" 


#replace ./app/src/main/ic_launcher-web.png with your icon then copy to res
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/res/mipmap-xxhdpi/ic_launcher.png
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/res/mipmap-xxxhdpi/ic_launcher.png
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/res/mipmap-xhdpi/ic_launcher.png
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/res/mipmap-mdpi/ic_launcher.png
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/res/mipmap-hdpi/ic_launcher.png
cp -v ./app/src/main/ic_launcher-web.png ./app/src/main/assets/icon.png

grep -ir com.example.app *|cut -d\: -f1|while read file;
do 
  sed -i "s/com.example.app/$appid/g" "$file"
done

sed -i "s/WebView App/$app/g" app/src/main/res/values/strings.xml

echo "window.location.replace('$url');" > "app/src/main/assets/index.js"
#sed -i "s|https://example.com|${url//&/\\&}|g" app/src/main/java/com/example/app/MainActivity.java
#sed -i "s|filmsbykris.com|$domain|g" app/src/main/java/com/example/app/MyWebViewClient.java

apk="app/build/outputs/apk/debug/app-debug.apk"
./gradlew build && adb install "$apk"
echo "$apk"
